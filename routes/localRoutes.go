package routes

import (
	"log"

	"bitbucket.com/country-api/endpoints"
	"github.com/gorilla/mux"
)

//Router retorna as rotas dos endpoints citdos
func Router() (router *mux.Router) {
	router = mux.NewRouter()

	log.Println("Endpoint [DELETE] /locals/{codigoTelefone} mapeado...")
	log.Println("Endpoint [PUT] /locals/{codigoTelefone} mapeado...")
	log.Println("Endpoint [GET] /locals/{codigoTelefone} mapeado...")
	log.Println("Endpoint [GET] /locals mapeado...")
	log.Println("Endpoint [POST] /locals mapeado...")

	router.HandleFunc("/locals/{codigoTelefone}", endpoints.DeleteLocalEndpoint).Methods("DELETE")
	router.HandleFunc("/locals/{codigoTelefone}", endpoints.PutLocalEndpoint).Methods("PUT")
	router.HandleFunc("/locals/{codigoTelefone}", endpoints.GetUniqueLocalEndpoint).Methods("GET")
	router.HandleFunc("/locals", endpoints.GetLocalsEndpoint).Methods("GET")
	router.HandleFunc("/locals", endpoints.PostLocalEndpoint).Methods("POST")

	return
}
