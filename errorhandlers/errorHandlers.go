package errorhandlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.com/country-api/models"
)

//HandleMessageError formata e retorna mensagem de erro por pesquisa
func HandleMessageError(w http.ResponseWriter, err error) {
	resultMessage := models.Result{}
	resultMessage.Message = fmt.Sprintf("Erro ao executar operação, consulte os logs da aplicação")
	resultMessage.Status = strconv.Itoa(http.StatusInternalServerError)
	logErrMsg := fmt.Sprintf("Erro - %s", err.Error())
	log.Println(logErrMsg)
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(resultMessage)
}
