package dao

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.com/country-api/models"
	"bitbucket.com/country-api/repository"
)

//FindByCodigoTelefone encontra um registro dado o código do telefone
func FindByCodigoTelefone(param string, value int) (local models.Local, err error) {
	sql := "select id, pais, cidade, codigoTelefone from place where " + param + " = ?"
	rows, err := repository.Db.Queryx(sql, value)
	if err != nil {
		var errorMsg = fmt.Sprintf("Não foi possivel executar a query %s - %v", sql, err.Error())
		err = errors.New(errorMsg)
		return
	}

	for rows.Next() {
		err = rows.StructScan(&local)
		if err != nil {
			var errorMsg = fmt.Sprint("Não foi possivel realizar o bind dos dados do banco na struct Local", err.Error())
			err = errors.New(errorMsg)
			return
		}
	}
	log.Printf("Retornando objeto do parametro %s de valor %d -> %+v", param, value, local)
	return
}

//FindAll retorna todos os registros de Locals
func FindAll() (locals []models.Local, err error) {
	sql := "select id, pais, cidade, codigoTelefone from place"
	rows, err := repository.Db.Queryx(sql)
	if err != nil {
		var errorMsg = fmt.Sprintf("Não foi possivel executar a query %s - %v", sql, err.Error())
		err = errors.New(errorMsg)
		return
	}

	for rows.Next() {
		local := models.Local{}
		err = rows.StructScan(&local)
		if err != nil {
			var errorMsg = fmt.Sprintf("Não foi possivel converter resultados da query %s - %v", sql, err.Error())
			err = errors.New(errorMsg)
			return
		}
		locals = append(locals, local)
	}
	log.Printf("Retornando %d registros da tabela 'place'", len(locals))
	return
}

//Save cadastra um novo registro de Local
func Save(local *models.Local) (result models.Result, err error) {
	sql := "insert into place (id, pais, cidade, codigoTelefone) values (?, ?, ?, ?)"
	_, err = repository.Db.Exec(sql, local.ID, local.Pais, local.Cidade, local.CodigoTelefonico)
	if err != nil {
		var errorMsg = fmt.Sprintf("Não foi possivel executar query [%s] com os valores [%+v] - %v", sql, local, err.Error())
		err = errors.New(errorMsg)
		return
	}

	result = models.Result{
		Status:  strconv.Itoa(http.StatusCreated),
		Message: "Registro salvo",
	}

	return
}

//Update atualiza os dados de um local
func Update(local *models.Local, id string) (result models.Result, err error) {
	sql := "update place set pais=:pais, cidade=:cidade, codigoTelefone=:codigoTelefone where id=:id"
	_, err = repository.Db.NamedExec(sql, map[string]interface{}{
		"pais":           local.Pais,
		"cidade":         local.Cidade,
		"codigoTelefone": local.CodigoTelefonico,
		"id":             id,
	})

	if err != nil {
		return
	}

	result = models.Result{
		Status:  strconv.Itoa(http.StatusOK),
		Message: "Registro atualizado",
	}
	return
}

//Delete remove um registro do banco
func Delete(id string) (result models.Result, err error) {
	sql := "delete from place where id=:id"
	_, err = repository.Db.NamedExec(sql, map[string]interface{}{
		"id": id,
	})

	result = models.Result{
		Status:  strconv.Itoa(http.StatusOK),
		Message: "Registro deletado",
	}

	return
}
