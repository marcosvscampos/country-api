package repository

import (
	/*
		github.com/go-sql-driver/mysql
		Import que chama só as funções init do pacote
	*/
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

//Db representa um objeto com conexão com o banco de dados
var Db *sqlx.DB

//OpenDBConnection função que abre a conexão com o banco MySQL
func OpenDBConnection() (err error) {
	Db, err = sqlx.Open("mysql", "root:manager@tcp(127.0.0.1)/cursodego")
	if err != nil {
		return
	}
	err = Db.Ping()
	if err != nil {
		return
	}

	return
}
