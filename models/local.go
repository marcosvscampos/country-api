package models

//Local armazena os dados da localidade no banco de dados
type Local struct {
	ID               string    `json:"id" db:"id"`
	Pais             string `json:"pais" db:"pais"`
	Cidade           string `json:"cidade" db:"cidade"`
	CodigoTelefonico int    `json:"codigoTelefone" db:"codigoTelefone"`
}
