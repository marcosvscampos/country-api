package models

//Result representa a mensagem de sucesso em formato JSON para ser retornada pelo servidor
type Result struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}
