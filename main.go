package main

import (
	"log"
	"net/http"

	"bitbucket.com/country-api/repository"
	"bitbucket.com/country-api/routes"
)

var (
	port = ":12345"
)

func main() {

	log.Println("Iniciando aplicação, conectando no banco de dados...")
	if err := repository.OpenDBConnection(); err != nil {
		log.Println("Erro ao conectar com o banco de dados -", err.Error())
		return
	}
	log.Println("Mapeando endpoints...")
	router := routes.Router()

	log.Println("Backend conectado na porta", port)
	if err := http.ListenAndServe(port, router); err != nil {
		log.Printf("Erro ao iniciar backend na porta %s - %s \r\n", port, err.Error())
		return
	}
}
