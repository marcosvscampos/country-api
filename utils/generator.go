package utils

import (
	"errors"
	"fmt"

	"bitbucket.com/country-api/models"
	uuid "github.com/satori/go.uuid"
)

//GenerateRandomID gera um UUID para colocar no Local
func GenerateRandomID(local *models.Local) (err error) {
	id, err := uuid.NewV4()
	if err != nil {
		errorMsg := fmt.Sprintf("Error ao gerar id do registro: %s", err.Error())
		err = errors.New(errorMsg)
		return
	}
	local.ID = id.String()
	return
}
