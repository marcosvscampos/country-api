create database cursodego;

create table place (
	id VARCHAR(128) NOT NULL,
	pais VARCHAR(50),
	cidade VARCHAR(50),
	codigoTelefone INT,
	PRIMARY KEY (id)
);

insert into place (id, pais, cidade, codigoTelefone) values ('c31d45ef-c863-4d20-9cd6-759bd701ee02', 'Finland', 'Loppi', 78);
insert into place (id, pais, cidade, codigoTelefone) values ('0f4dcc49-cd60-4481-8177-9f4b60e0a403', 'France', 'Blois', 1);
insert into place (id, pais, cidade, codigoTelefone) values ('ebaae8e6-3874-4ded-a47d-007c229526e5', 'Sweden', 'Göteborg', 79);
insert into place (id, pais, cidade, codigoTelefone) values ('222428dc-8462-484a-ba2b-de5ca59ad7e6', 'Indonesia', 'Pajeksan', 8);
