package endpoints

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"bitbucket.com/country-api/dao"
	"bitbucket.com/country-api/utils"

	"bitbucket.com/country-api/models"

	"bitbucket.com/country-api/errorhandlers"
	"github.com/gorilla/mux"
)

//DeleteLocalEndpoint atualiza um local existente no banco
func DeleteLocalEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r)
	codigoTelefone, err := strconv.Atoi(params["codigoTelefone"])
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}

	localFromDB, err := dao.FindByCodigoTelefone("codigoTelefone", codigoTelefone)
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}

	message, err := dao.Delete(localFromDB.ID)
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}
	log.Printf("Registro [%s] deletado com sucesso!", localFromDB.ID)
	json.NewEncoder(w).Encode(message)
}

//PutLocalEndpoint atualiza um local existente no banco
func PutLocalEndpoint(w http.ResponseWriter, r *http.Request) {
	localFromRequest := models.Local{}
	err := json.NewDecoder(r.Body).Decode(&localFromRequest)
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r)
	codigoTelefone, err := strconv.Atoi(params["codigoTelefone"])
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}

	localFromDB, err := dao.FindByCodigoTelefone("codigoTelefone", codigoTelefone)
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}

	message, err := dao.Update(&localFromRequest, localFromDB.ID)
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}
	log.Printf("Registro [%+v] atualizado com sucesso!", localFromDB)
	json.NewEncoder(w).Encode(message)
}

//PostLocalEndpoint cria um local novo
func PostLocalEndpoint(w http.ResponseWriter, r *http.Request) {
	local := models.Local{}
	err := utils.GenerateRandomID(&local)
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}

	w.Header().Set("content-type", "application/json")
	err = json.NewDecoder(r.Body).Decode(&local)
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}

	message, err := dao.Save(&local)
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}
	log.Printf("Registro [%+v] salvo com sucesso!", local)
	json.NewEncoder(w).Encode(message)
}

//GetUniqueLocalEndpoint retorna um Local dado o codigo do telefone
func GetUniqueLocalEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r)
	codigoTelefone, err := strconv.Atoi(params["codigoTelefone"])
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}
	local, err := dao.FindByCodigoTelefone("codigoTelefone", codigoTelefone)
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
		return
	}
	json.NewEncoder(w).Encode(local)
}

//GetLocalsEndpoint retorna todos os locais cadastrados
func GetLocalsEndpoint(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "appication/json")
	locals, err := dao.FindAll()
	if err != nil {
		errorhandlers.HandleMessageError(w, err)
	}

	json.NewEncoder(w).Encode(locals)
}
